package whapp;

import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;

public class FirstTest {

	private static AndroidDriver driver;

	@BeforeSuite
	public static void beforeSuite() throws MalformedURLException {

		DesiredCapabilities capability = new DesiredCapabilities();
		capability.setCapability("deviceName", "Nexus 5");
		capability.setCapability("platformName", "Android");
		capability.setCapability("platformVersion", "6.0.1");

		capability.setCapability("app", "D:\\workspace\\FirstWhApp\\apk\\WhatsApp.apk");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capability);
	}

	@Parameters({ "noOfTimes" })
	@Test
	public static void sendMessageToTheo(int noOfTimes) throws MalformedURLException {

		driver.findElementByXPath(
				"//android.widget.TextView[contains(@resource-id, 'com.whatsapp:id/conversations_row_contact_name') and @text='Theo InCrys']")
				.click();
		driver.findElement(By.className("android.widget.EditText")).click();

		for (int i = 0; i < noOfTimes; i++) {
			driver.findElementByXPath("//android.widget.EditText[contains(@resource-id, 'com.whatsapp:id/entry')]")
					.sendKeys("*Automated Message sent from Eclipse*");
			driver.findElementByXPath("//android.widget.ImageButton[@content-desc='Send']").click();
		}
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

}
